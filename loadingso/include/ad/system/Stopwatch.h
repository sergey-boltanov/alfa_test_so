﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_System_Stopwatch_h
#define AD_System_Stopwatch_h

#include <ad/system/Types.h>


namespace AD
{
	namespace System
	{
		/// \br Класс для высокоточного замера промежутков времени
		class AD_System_API Stopwatch
		{
		public:

			Stopwatch(bool isStarted = false);
			explicit Stopwatch(Int64 startTicks);

			/// \br Начать отсчет
			void start();
			/// \br Остановить отсчет
			void stop();
			/// \br Сбросить счетчики
			void reset();
			/// \br Перезапустить отсчет
			void restart();

			/// \br Возвращает время прошедшее между началом и концном отсчета времени в микросекундах
			Timespan elapsed() const;
			/// \br Возвращает время прошедшее между началом и концном отсчета времени в тиках
			Int64 elapsedTicks() const;
			/// \br Возвращает количество тиков соответсвующее началу отсчета
			Int64 getStartTicks() const;
			
			/// \br Возвращает количество тиков в одной секунде
			static Int64 getFrequency();


		private:
			
			Int64 _startTicks;
			Int64 _stopTicks;
		};


		/*!
		\class Stopwatch Stopwatch.h <ad/system/Stopwatch.h>
		*/
	}
}

#endif
