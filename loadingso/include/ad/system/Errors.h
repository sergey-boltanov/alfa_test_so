﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_System_Errors_h
#define AD_System_Errors_h

#include <ad/system/format.h>
#include <ad/system/Types.h>
#include <ad/system/System.h>
#include <ad/system/Platform.h>

#include <Poco/Debugger.h>
#include <Poco/NestedDiagnosticContext.h>

#define AD_GET_PLACE (AD::System::format("{0}:{1}", __FILE__, __LINE__))

#define AD_ERROR(Exception, ...) Exception(AD_GET_PLACE, AD::System::format(__VA_ARGS__))
#define AD_WRAPPED_ERROR(Exception, Nested, ...) Exception(Nested, AD_GET_PLACE, AD::System::format(__VA_ARGS__))

#define AD_ERROR_WITH_CODE(Exception, Code, ...) Exception(AD_GET_PLACE, AD::System::format(__VA_ARGS__), Code)
#define AD_WRAPPED_ERROR_WITH_CODE(Exception, Nested, Code, ...) Exception(Nested, AD_GET_PLACE, AD::System::format(__VA_ARGS__), Code)

#define AD_NDC(...) Poco::NDCScope _theNdcScope(AD::System::format(__VA_ARGS__), __LINE__, __FILE__)
#if AD_OS == AD_ANDROID
#include <ad/system/NDK.h>
#define AD_ASSERT(Condition, ...) if (!(Condition)) { const AD::System::AssertError error(#Condition, AD_GET_PLACE, AD::System::format(__VA_ARGS__)); LOG((std::string("Assertion violation: ") + error.getDisplayText()).c_str()); throw error; } else (void) 0
#else
#define AD_ASSERT(Condition, ...) if (!(Condition)) { const AD::System::AssertError error(#Condition, AD_GET_PLACE, AD::System::format(__VA_ARGS__)); Poco::Debugger::enter(std::string("Assertion violation: ") + error.getDisplayText()); throw error; } else (void) 0
#endif

#define AD_DECLARE_STANDARD_ERROR_CTOR(Name, Code) \
	Name(const AD::System::String& place, const AD::System::String& reason = AD::System::String(), int code = Code); \
	Name(const AD::System::Error& error, const AD::System::String& place, const AD::System::String& reason = AD::System::String(), int code = Code); \
	Name(const Poco::Exception& exception, const AD::System::String& place, const AD::System::String& reason = AD::System::String(), int code = Code);
#define AD_DECLARE_COPY_ERROR_CTOR(Name) \
	Name(const Name& error); \
	Name& operator = (const Name& error);
#define AD_DECLARE_COMMON_ERROR_STUFF(Name) \
	Name(); \
	virtual ~Name() throw(); \
	virtual const char* name() const throw(); \
	virtual const char* className() const throw(); \
	virtual AD::System::Error* clone() const; \
	virtual void rethrow() const;
#define AD_DECLARE_ERROR_CLASS(API, Name, Base) struct API Name : public Base

#define AD_DECLARE_ERROR_WITH_CODE(API, Name, Base, Code) \
	AD_DECLARE_ERROR_CLASS(API, Name, Base) \
	{ \
		AD_DECLARE_STANDARD_ERROR_CTOR(Name, Code) \
		AD_DECLARE_COPY_ERROR_CTOR(Name) \
		AD_DECLARE_COMMON_ERROR_STUFF(Name) \
	};

#define AD_DECLARE_ERROR(API, Name, Base)  AD_DECLARE_ERROR_WITH_CODE(API, Name, Base, 0)

#define AD_IMPLEMENT_STANDARD_ERROR_CTOR(Name, Base) \
	Name::Name(const AD::System::String& place, const AD::System::String& reason, int code) : Base(place, reason, code) {} \
	Name::Name(const AD::System::Error& error, const AD::System::String& place, const AD::System::String& reason, int code) : Base(error, place, reason, code) {} \
	Name::Name(const Poco::Exception& exception, const AD::System::String& place, const AD::System::String& reason, int code) : Base(exception, place, reason, code) {}
#define AD_IMPLEMENT_COPY_ERROR_CTOR(Name, Base) \
	Name::Name(const Name& error) : Base(error) {} \
	Name& Name::operator =(const Name& error) { Base::operator =(error); return *this; }
#define AD_IMPLEMENT_COMMON_ERROR_STUFF(Name, Description) \
	Name::Name(){} \
	Name::~Name() throw() {} \
	const char* Name::name() const throw() { return Description; } \
	const char* Name::className() const throw() { return typeid(*this).name(); }; \
	AD::System::Error* Name::clone() const { return new Name(*this); } \
	void Name::rethrow() const { throw *this; }
#define AD_IMPLEMENT_ERROR(Name, Base, Description) \
	AD_IMPLEMENT_STANDARD_ERROR_CTOR(Name, Base) \
	AD_IMPLEMENT_COPY_ERROR_CTOR(Name, Base) \
	AD_IMPLEMENT_COMMON_ERROR_STUFF(Name, Description)


namespace AD
{
	namespace System
	{
		AD_DECLARE_ERROR_CLASS(AD_System_API, Error, Poco::Exception)
		{
			AD_DECLARE_STANDARD_ERROR_CTOR(Error, 0)
			AD_DECLARE_COPY_ERROR_CTOR(Error)
			AD_DECLARE_COMMON_ERROR_STUFF(Error)

			virtual const Error* getNested() const;
			virtual String getReason() const;
			virtual int getCode() const;
			virtual String getDisplayText() const;

		protected:

			void addDetails(const String& message);


		private:

			struct Impl;
			Impl* _impl;
		};

		AD_DECLARE_ERROR_CLASS(AD_System_API, AssertError, Error)
		{
			AssertError(const String& condition, const String& place, const String& reason);
			AD_DECLARE_COPY_ERROR_CTOR(AssertError)
			AD_DECLARE_COMMON_ERROR_STUFF(AssertError)
		};

		AD_DECLARE_ERROR(AD_System_API, SystemError, Error);

		AD_DECLARE_ERROR(AD_System_API, EnumError, SystemError);
		AD_DECLARE_ERROR(AD_System_API, UnexpectedValue, EnumError);
		AD_DECLARE_ERROR(AD_System_API, UnexpectedCode, EnumError);
	}
}

#endif
