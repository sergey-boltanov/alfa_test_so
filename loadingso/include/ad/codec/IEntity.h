﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_Codec_IEntity_h
#define AD_Codec_IEntity_h

#include <ad/codec/ISerializable.h>
#include <ad/codec/EntityType.h>
#include <ad/codec/Codec.h>

#include <ad/system/IStringable.h>
#include <ad/system/Types.h>
#include <ad/system/Delegate.h>

#include <vector>

#define AD_DECLARE_ENTITY_GETTYPEID virtual AD::Codec::EntityType getTypeId() const; static AD::Codec::EntityType getType();
#define AD_DECLARE_ENTITY_CLONE virtual AD::Codec::IEntity::Ptr clone() const;
#define AD_DECLARE_ENTITY_INTERFACE AD_DECLARE_ENTITY_GETTYPEID AD_DECLARE_ENTITY_CLONE
#define AD_DECLARE_ENTITY_STUF(Type) AD_DECLARE_TYPE_INFO AD_DECLARE_CTOR(Type) AD_DECLARE_SERIALIZABLE_INTERFACE AD_DECLARE_STRINGABLE_INTERFACE AD_DECLARE_ENTITY_INTERFACE


namespace AD
{
	namespace Codec
	{
		 /// \br Представляет команду и ответ которые служат для взаимодействия между сервером и клиентом.
		 /// \be Represents request and response which serve for communication between server and client.
		class IEntity
			: virtual public ISerializable
			, virtual public System::IStringable
		{
		public:

			typedef Poco::SharedPtr<IEntity> Ptr;
			typedef Delegate<Ptr (EntityType)> FactoryMethod;

			typedef std::vector<Ptr> List;


		public:

			/// \br Создает копию ентити
			/// \be Returns entity copy
			virtual Ptr clone() const = 0;
			/// \br Возвращает код ентити
			/// \be Returns entity code
			virtual EntityType getTypeId() const = 0;

			AD_Codec_API virtual String toString(const String& format = String()) const;

			/// \br Создает ентити по ее коду
			/// \be Creates entity by code
			AD_Codec_API static Ptr create(EntityType typeId);
			/// \br Регистрирует ентити соответсвующего типа в качестве шаблона, для последующего создания копий
			/// \be Register entity corresponding type as template, by code
			AD_Codec_API static Ptr registerType(const Ptr& item);
			/// \br Проверяет зарегистрированна ли ентити
			AD_Codec_API static bool isRegistered(EntityType typeId);


		protected:

			virtual ~IEntity(){};

			friend class Poco::ReleasePolicy<IEntity>;
		};

		typedef IEntity::List EntityList;
		typedef IEntity::Ptr EntityPtr;

		/*!
		\class IEntity IEntity.h <ad/codec/IEntity.h>

		\~english 
		Any class represents request or response, which transfered between client and server, must implement this interface.
		

		\russian 
		Любой класс, который описывает команду или ответ передаваемые между клиентом и сервером, должен реализовывать этот интерфейс.
		*/
	}
}

#endif
