﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_Data_AuthenticationRequestEntity_h
#define AD_Data_AuthenticationRequestEntity_h

#include <ad/data/ClientType.h>
#include <ad/data/Types.h>
#include <ad/data/Data.h>

#include <ad/codec/IEntity.h>
#include <ad/system/OSType.h>

namespace AD
{
	namespace Data
	{
		/// \br Cлужит для аутентификации пользователя
		struct AD_Data_API AuthenticationRequestEntity
			: virtual public Codec::IEntity
		{
			String login;
			String password;
			/// \br Версия клиентского терминала
			String clientVersion;
			/// \br Код разработчика 
			String developerCode;
			/// \br Версия протокола на клиенте
			String protocolVersion;
			/// \br Версия операционной системы
			String osVersion;
			/// \br Тип операционной системы
			System::OSType osType;
			/// \br Наименование модели устройства
			String deviceModel;
			/// \br MAC адрес сетевого интерфейса по которому установленно соединение к фронтенду 1 зоны
			String macAdress;
			/// \br Уникальный иденификатор устройства
			String idDevice;
			/// \br Тип терминала
			ClientType clientType;
			/// \br Время на устройстве клиента
			Timestamp localTime;
			/// \br Каталог в котором установлено приложение
			String homeDir;
			/// \br Рабочий каталог
			String filesDir;
			/// \br Имя учетной записи на устройстве
			String userName;
			/// \br Внешний IP адрес, к котому подключается клиент
			String connectionIP;

			AD_DECLARE_ENTITY_STUF(AuthenticationRequestEntity);
		};
	}
}

#endif
