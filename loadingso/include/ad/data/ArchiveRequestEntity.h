﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_Data_ArchiveRequestEntity_h
#define AD_Data_ArchiveRequestEntity_h

#include <ad/data/CandleType.h>
#include <ad/data/TimeResolution.h>
#include <ad/data/TimeFrameInfoEntity.h>
#include <ad/data/Data.h>

#include <ad/codec/IEntity.h>


namespace AD
{
	namespace Data
	{
		/// \br Предназначена для запроса истории по графикам
		/// \be Serves for request chart history
		struct AD_Data_API ArchiveRequestEntity
			: virtual public Codec::IEntity
		{
			/// \br Тип запрашиваемых свеч
			CandleType candleType;
			/// \br Номер клиентского запроса
			/// \be Identifier user request
			Int32 idRequest;
			/// \copybrief FinInstrumentEntity::id
			Int32 idFI;
			/// \br Разрешение графика
			TimeResolution timeResolution;
			/// \br Список контрольных сумм данных на клиенте
			/// \be List of checksum client data
			TimeFrameInfoEntity::List timeFramesInfo;
			/// \br Базовая дата запроса относительно которой вычисляются границы запрашиваемого диапазона
			Timestamp date;
			/// \br Количество запрашиваемых торговых дней, больше ноля запрос в будущее относительно date, меньше ноля - в прошлое
			/// \be Number of requested days
			Int32 daysCount;

			AD_DECLARE_ENTITY_STUF(ArchiveRequestEntity);

			/// \br Максимальное количество запрашиваемых торговых дней в зависимости от типа свеч и разрешения графика, смотри таблицу
			static Int32 getMaxDaysCount(CandleType candleType, TimeResolution timeResolution);
		};


		/*!
		\request{ArchiveRequestEntity}
		\ingroup history
		\frontends BirzArchAndMediaServer
		\responses ServerAnswerEntity LimitsOverflowEntity

		|     | S |  M  |  H  |
		|-----|---|-----|-----|
		| MPV | 0 |  2  |  5  |
		| Std | 1 | 30  | 720 |
		- MPV - CandleType::MPV
		- Std - CandleType::Standard
		- S - TimeResolution::Second
		- M - TimeResolution::Minute
		- H - TimeResolution::Hour
		
		\~russian
		В таблице, значение на пересечении типа свечи и разрешения графика равно число максимально запрашиваемых дней одним запросом.

		Все запросы выполняются в днях. Т.е. минимальный промежуток времени, который можно запросить, независимо от разрешения это 1 день.
		Если на клиенте имеется часть данных, то следует послать контрольную сумму этих данных. В случае не совпадения контрольных сумм на сервере и на
		клиенте клиент получит обновленые данные, в противном случае в ответе сервера такой день будет отсутствовать и должны использоваться данные клиента.
		*/
	}
}

#endif