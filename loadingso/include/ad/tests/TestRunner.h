#ifndef AD_Tests_TestRunner_h
#define AD_Tests_TestRunner_h

#include "ad/tests/Tests.h"
#include "ad/tests/IService.h"
#include <Poco/SimpleFileChannel.h>
#include <Poco/Message.h>

namespace AD
{
	namespace Tests
	{
		class TestRunnerChannel
			: public Poco::Channel
		{
		public:
			TestRunnerChannel(const IService& service);

			static std::string priorityToString(const Poco::Message::Priority& priority);

			virtual void log(const Poco::Message& msg);
		private:
			const IService& _service;
		};
	}
}

#endif
