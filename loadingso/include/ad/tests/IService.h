#ifndef AD_Tests_IService_h
#define AD_Tests_IService_h

#include "ad/tests/Tests.h"
#include <string>

namespace AD
{
	namespace Tests
	{

		class IService
		{
		public:
			virtual void logInfo(const std::string& text) const = 0;
			virtual void logError(const std::string& text) const = 0;
			virtual void logWarning(const std::string& text) const = 0;
			virtual void logAssert(const std::string& text) const = 0;
			virtual void logFatal(const std::string& text) const = 0;

			virtual void logTestResult(const std::string& text) const = 0;

		protected:
			virtual ~IService(){}
		};

		AD_Tests_API void initLogger(const IService& service, const std::string& logLevel);

		AD_Tests_API bool runTests(const IService& service, const std::string& configPath);

		AD_Tests_API void runWatchDog(const std::string& configPath);
		AD_Tests_API void stopWatchDog();
	}
}

#endif