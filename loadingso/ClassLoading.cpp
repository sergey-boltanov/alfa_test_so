#include <dlfcn.h>
#include <iostream>
#include "helloAlfa.h"

using namespace std;

int main(int argc, char **argv)
{
	cout << "symbol resolved: \n";
  	/* on Linux, use "./myclass.so" */
  	void* handle = dlopen("./lib/helloAlfa.so", RTLD_LAZY);
	
	cout << "Hello, world! 0\n";

	HelloAlfa* (*create)();
	cout << "Hello, world! 1\n";

	create = (HelloAlfa* (*)())dlsym(handle, "create_object");
	cout << "Hello, world! 2\n";

	HelloAlfa* myClass = (HelloAlfa*)create();
  	myClass->DoSomething();
}
